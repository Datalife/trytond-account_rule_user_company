datalife_account_rule_user_company
==================================

The account_rule_user_company module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_rule_user_company/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_rule_user_company)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
