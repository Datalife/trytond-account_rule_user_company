# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool


def register():
    Pool.register(
        module='account_rule_user_company', type_='model')
    Pool.register(
        module='account_rule_user_company', type_='wizard')
    Pool.register(
        module='account_rule_user_company', type_='report')
